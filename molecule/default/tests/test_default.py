import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directory_exists(host):
    file1 = host.file("/var/www/test")
    file2 = host.file("/var/www/html")
    assert file1.exists or file2.exists


def test_nginx_access(host):
    # 403 returned because of an empty directory in the web server
    cmd = host.run('curl http://localhost')
    assert "403 Forbidden" in cmd.stdout


def test_nginx_running_and_enabled(host):
    nginx = host.service("nginx")
    assert nginx.is_running
    assert nginx.is_enabled
